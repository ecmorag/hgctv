#include "soc-hw.h"

int main(int argc, char **argv){
	uint32_t i = 0;
	for(i = 0; i < 32; i++){
		ram_write(i, (char)i);
	}
	for(i = 31; i >= 0; i--){
		gpio0 -> out = ram_read(i);
		msleep(1000);
	}
	return 0;
}

