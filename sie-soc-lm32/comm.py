#!/usr/bin/env python
import sys
import serial
import time

s = serial.Serial("/dev/ttyUSB0")
s.baudrate = 115200

while 1:
	s.flushInput()

	print "Insert command:\n"

	x = s.read(4)

	print "Writing file..."

	f = open("prueba.txt","w")

	for i in x:
		i = bin(ord(i)+256)[3:]
		f.writelines(i+'\n')
		#print i

	print x

	f.close()

	print "\nReading file..."

	f = open("prueba.txt","r")

	y = ""

	for i in range(0,4):
		i = f.readline()[:8]
		num = 0
		n = 7

		for j in range(0,8):
			num = num + int(i[n])*2**j
			n = n - 1

		#print bin(num+256)[3:]
		y = y + chr(num)

	print y

	print "Transmitting..."

	for i in y:
		s.write(i)
	#	print i
#		time.sleep(1)

	print "ok"

	f.close()
	s.flushOutput()

	q = raw_input("q -> quit\nENTER -> continue: ")

	if q == 'q':
		s.close()
		break
