module wb_pwm(
	input              clk,
	input              rst,
	// Wishbone interface
	input              wb_stb_i,
	input              wb_cyc_i,
	output             wb_ack_o,
	input              wb_we_i,
	input       [31:0] wb_adr_i,
	input        [3:0] wb_sel_i,
	input       [31:0] wb_dat_i,
	output reg  [31:0] wb_dat_o,
	output pulse
);

reg en;
reg [31:0] high_cycles;
reg [31:0] low_cycles;

wire cyc_ok;

pwm pwm0(
	.clk(clk),
	.rst(rst),

	.en(en),
	.high_cycles(high_cycles),//219
	.low_cycles(low_cycles),//437

	.cyc_ok(cyc_ok),
	.pulse(pulse) //carrier signal 38KHz
);

//---------------------------------------------------------------------------
// 
//---------------------------------------------------------------------------

// Wishbone
reg  ack;
assign wb_ack_o = wb_stb_i & wb_cyc_i & ack;

wire wb_rd = wb_stb_i & wb_cyc_i & ~wb_we_i;
wire wb_wr = wb_stb_i & wb_cyc_i &  wb_we_i;

always @(posedge clk) begin
	if (rst) begin
		ack <= 0;
	end else begin
		// Handle WISHBONE access
		ack <= 0;

		if (wb_rd & ~ack) begin           // read cycle
			ack <= 1;
			case (wb_adr_i[7:0])
			'h00: begin wb_dat_o[0] <= cyc_ok; wb_dat_o[31:1] <= 0; end
			default: wb_dat_o <= 32'b0;
			endcase
		end else if (wb_wr & ~ack ) begin // write cycle
			ack <= 1;
			case (wb_adr_i[7:0])
			'h04: en <= wb_dat_i[0];
			'h08: high_cycles <= wb_dat_i;
			'h0C: low_cycles <= wb_dat_i;
			endcase
		end
	end
end

endmodule
