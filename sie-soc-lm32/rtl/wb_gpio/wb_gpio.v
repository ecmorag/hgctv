//---------------------------------------------------------------------------
// Wishbone General Pupose IO Component
//
//     0x00	
//     0x10     gpio_in    (read-only)
//     0x14     gpio_out   (read/write)
//     0x18     blink_out    (read/write)
//
//---------------------------------------------------------------------------

module wb_gpio (
	input              clk,
	input              reset,
	// Wishbone interface
	input              wb_stb_i,
	input              wb_cyc_i,
	output             wb_ack_o,
	input              wb_we_i,
	input       [31:0] wb_adr_i,
	input        [3:0] wb_sel_i,
	input       [31:0] wb_dat_i,
	output reg  [31:0] wb_dat_o,
	//
	output             intr,
	// IO Wires
	input       [31:0] gpio_in,
	output 		[31:0] gpio_out
);

//---------------------------------------------------------------------------
// 
//---------------------------------------------------------------------------

wire [31:0] gpiocr = 32'b0;
reg  [31:0] blink_out;
reg en;
reg [31:0]tmp;

// Wishbone
reg  ack;
assign wb_ack_o = wb_stb_i & wb_cyc_i & ack;

wire wb_rd = wb_stb_i & wb_cyc_i & ~wb_we_i;
wire wb_wr = wb_stb_i & wb_cyc_i &  wb_we_i;

always @(posedge clk)
begin
	if (reset) begin
		ack      <= 0;
		tmp <= 'b0;
		en <= 0;
		blink_out <= 0;
	end else begin
		// Handle WISHBONE access
		ack    <= 0;

		if (wb_rd & ~ack) begin           // read cycle
			ack <= 1;

			case (wb_adr_i[7:0])
			'h00: wb_dat_o <= gpiocr;
			'h10: wb_dat_o <= gpio_in;
			'h14: wb_dat_o <= tmp;
			'h18: wb_dat_o <= blink_out;
			default: wb_dat_o <= 32'b0;
			endcase
		end else if (wb_wr & ~ack ) begin // write cycle
			ack <= 1;

			case (wb_adr_i[7:0])
			'h00: en <= wb_dat_i[0];
			'h14: tmp <= wb_dat_i;
			'h18: blink_out  <= wb_dat_i;
			endcase
		end
	end
end

//********************************************************
//Blinker extension
reg [23:0] counter;
wire blink;

always@(posedge clk or posedge reset) begin
	if(reset) counter <= 24'b0;
	else counter <= counter + 1;
end 

assign blink = counter[23];

assign gpio_out[0] = (en & blink_out[0])? blink : tmp[0] ;
assign gpio_out[1] = (en & blink_out[1])? blink : tmp[1] ;
assign gpio_out[2] = (en & blink_out[2])? blink : tmp[2] ;
assign gpio_out[3] = (en & blink_out[3])? blink : tmp[3] ;
assign gpio_out[4] = (en & blink_out[4])? blink : tmp[4] ;
assign gpio_out[5] = (en & blink_out[5])? blink : tmp[5] ;
assign gpio_out[6] = (en & blink_out[6])? blink : tmp[6] ;
assign gpio_out[7] = (en & blink_out[7])? blink : tmp[7] ;
assign gpio_out[8] = (en & blink_out[8])? blink : tmp[8] ;
assign gpio_out[9] = (en & blink_out[9])? blink : tmp[9] ;
assign gpio_out[10] = (en & blink_out[10])? blink : tmp[10] ;
assign gpio_out[11] = (en & blink_out[11])? blink : tmp[11] ;
assign gpio_out[12] = (en & blink_out[12])? blink : tmp[12] ;
assign gpio_out[13] = (en & blink_out[13])? blink : tmp[13] ;
assign gpio_out[14] = (en & blink_out[14])? blink : tmp[14] ;
assign gpio_out[15] = (en & blink_out[15])? blink : tmp[15] ;
assign gpio_out[16] = (en & blink_out[16])? blink : tmp[16] ;
assign gpio_out[17] = (en & blink_out[17])? blink : tmp[17] ;
assign gpio_out[18] = (en & blink_out[18])? blink : tmp[18] ;
assign gpio_out[19] = (en & blink_out[19])? blink : tmp[19] ;
assign gpio_out[20] = (en & blink_out[20])? blink : tmp[20] ;
assign gpio_out[21] = (en & blink_out[21])? blink : tmp[21] ;
assign gpio_out[22] = (en & blink_out[22])? blink : tmp[22] ;
assign gpio_out[23] = (en & blink_out[23])? blink : tmp[23] ;
assign gpio_out[24] = (en & blink_out[24])? blink : tmp[24] ;
assign gpio_out[25] = (en & blink_out[25])? blink : tmp[25] ;
assign gpio_out[26] = (en & blink_out[26])? blink : tmp[26] ;
assign gpio_out[27] = (en & blink_out[27])? blink : tmp[27] ;
assign gpio_out[28] = (en & blink_out[28])? blink : tmp[28] ;
assign gpio_out[29] = (en & blink_out[29])? blink : tmp[29] ;
assign gpio_out[30] = (en & blink_out[30])? blink : tmp[30] ;
assign gpio_out[31] = (en & blink_out[31])? blink : tmp[31] ;

endmodule
