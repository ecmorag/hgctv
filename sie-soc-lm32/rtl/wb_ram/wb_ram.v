module wb_ram(
	input              clk,
	input              rst,
	// Wishbone interface
	input              wb_stb_i,
	input              wb_cyc_i,
	output             wb_ack_o,
	input              wb_we_i,
	input       [31:0] wb_adr_i,
	input        [3:0] wb_sel_i,
	input       [31:0] wb_dat_i,
	output reg  [31:0] wb_dat_o
);

reg we;
reg en;
reg [13:0] addr;
reg [7:0] di;
wire [7:0] do;

ram ram0(
	.clk(clk),
	.we(we),
	.en(en),
	.addr(addr),
	.di(di),
	.do(do)
);


//---------------------------------------------------------------------------
// 
//---------------------------------------------------------------------------

// Wishbone
reg  ack;
assign wb_ack_o = wb_stb_i & wb_cyc_i & ack;

wire wb_rd = wb_stb_i & wb_cyc_i & ~wb_we_i;
wire wb_wr = wb_stb_i & wb_cyc_i &  wb_we_i;

always @(posedge clk)
begin
	if (rst) begin
		ack <= 0;
		en <= 'b0;
		we <= 'b0;
		addr <= 'b0;
		di <= 'b0;
		wb_dat_o<= 'b0;
	end else begin
		// Handle WISHBONE access
		ack <= 0;
		if (wb_rd & ~ack) begin           // read cycle
			ack <= 1;
			case (wb_adr_i[7:0])
			'h00: begin  wb_dat_o[31:16] <= 0; wb_dat_o[15:0] <= do; end
			default: wb_dat_o <= 32'b0;
			endcase
		end else if (wb_wr & ~ack ) begin // write cycle
			ack <= 1;
			case (wb_adr_i[7:0])
			'h04: begin en <= wb_dat_i[31]; we <= wb_dat_i[30]; addr <= wb_dat_i[21:8]; di <= wb_dat_i[7:0]; end //escribir ram
			'h08: begin en <= wb_dat_i[31]; we <= wb_dat_i[30]; addr <= wb_dat_i[21:8]; end //leer ram
			default: we <= 0;
			endcase
		end
	end
end

endmodule
