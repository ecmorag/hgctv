module ram(
	input clk,
	input we,
	input en,
	input [13:0] addr,
	input [7:0] di,
	output reg [7:0] do
);

reg [7:0] mem [16383:0];//ram block

always@(posedge clk) begin
	if(en) begin
		if(we) mem[addr] <= di;
		else do <= mem[addr];
	end
end

endmodule
